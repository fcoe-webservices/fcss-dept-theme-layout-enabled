
(function ($, Drupal) {
  $(document).foundation();
  
  Drupal.behaviors.myBehavior = {
    attach: function (context, settings) {

      $('#layout-builder-content-preview').change(function() {
        if(!$(this).is(":checked")) {
          //if(!$("#layout-builder").hasClass("layout-builder--content-preview-disabled")) {
            $("#layout-builder").addClass("layout-builder--content-preview-disabled");
          console.log('turned off');
          //} else {
            //$("#layout-builder").removeClass("layout-builder--content-preview-disabled");
          } else {
            console.log('turned on');
            $("#layout-builder").removeClass("layout-builder--content-preview-disabled");
          }
        //}
      });

      jQuery( ".videoURL" ).click(function(event) {
        event.preventDefault();
        vall = jQuery(this).attr("href");
        var videoid = vall.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
        jQuery('#videoModal iframe').attr('src', "https://www.youtube.com/embed/" + videoid[1] + "?autoplay=1&rel=0" );
        jQuery('#videoModal').foundation('open');
      });


      jQuery(document).on('closed.zf.reveal', '[data-reveal]', function () {
        jQuery('#videoModal iframe').attr('src', '');
      });

    }
  };

})(jQuery, Drupal);
