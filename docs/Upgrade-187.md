# Upgrading from 185 to 187 #

## Build the Lando Environment ##

Use the following lando file to build the initial environment for the 187 build. You can change the local site name and ports to be anything you like.


```
name: dept187
recipe: drupal8
webroot: .
proxy:
  appserver:
    - YOURPREFERREDURL.lndo.site

services:
  node:
    type: node
    overrides:
      ports:
        - 8014:8014
    globals:
      gulp-cli: latest

  database:
    overrides:
      command: /lando-entrypoint.sh /opt/bitnami/scripts/mysql/entrypoint.sh /opt/bitnami/scripts/mysql/run.sh
config:
  drush: ^8

tooling:
  npm:
    service: node
  node:
    service: node
  gulp:
    service: node

```


## Build the 01.07_c site ##

Use the make file within the `Make Files` repo on Bitbucket. Be sure to set the referenced repos to public for the makefile to work correctly.

```
lando drush make https://bitbucket.org/fcoe-webservices/make-files/raw/db5a211faa034584028e6b84c414781e68f0b03a/fcss_dept_01_07.make
```

Remember to include `lando` at the beginning of the file

## Configure the default 01.07 Platform site ##

The 01.07 platform has a starter site located at `sites/dev.starter-clone-test.o1.its-hosting-187.fcoe.org', and it helps to have this site in your local dev environment for reference. To get this set up, follow these steps. Here's an example of the workflow for the url 'dept187.lando.site'

1) Rsync down the `static/fcss_dept_01.07_c/sites/dev.starter-clone-test.o1.its-hosting-187.fcoe.org` folder from the 187 server

2) Swap out the BOA generated settings file with appropriate settings file from whatever version of Drupal you're using, (i.e. 8.9.11)

3) Change the database credentials at the bottom to read

```
$databases['default']['default'] = array (
  'database' => 'drupal8',
  'username' => 'drupal8',
  'password' => 'drupal8',
  'prefix' => '',
  'host' => 'database',
  'port' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
```

4) Change the sites/sites.php file to reflect your new lando URL and site folder

```
$sites = array (
  'dept187.lndo.site' => 'dev.starter-clone-test.o1.its-hosting-187.fcoe.org',
);
```

5. Import the DB from the `dev.starter-clone-test.o1.its-hosting-187.fcoe.org` site on production

## Importing a Production Site for Migration ##

Existing sites on the 01.03 platform on the 185 server need to be both upgraded and migrated to the 187 server.

The simplest way to migrate sites is to pull them into the 187 platform locally (set up above), then upgrade (see below). Storing all sites in a single lando instance allows all sites to function as though they were in the same platform. This is essentially the setup you'd have for a drupal multisite environment, but withi lando.

To add an additional site follow these steps

1) Add the site URL to the appserver config in the lando file

```
proxy:
  appserver:
    - dept187.lndo.site
    - NEWSITE.lndo.site
```

2) Rebuild Lando

```
lando rebuild
```

3) log in to the mysql instance and create a database. The name of this database will need to be reflected in the settings.php database credentials for the new site. The database name itself is unimportant

```
lando mysql
CREATE DATABASE database_name
```

4) Give the drupal8 user access to the new database

```
GRANT ALL PRIVILEGES ON database_name.* TO 'drupal8'@'%';
```

5) Pull the database and `sites/SITENAME.fcoe.org` folder on the 185 server into the local build

6) Replace the settings.php file and update the database credentials. Note that you will be using a different database name, not `drupal8`. The new database name will be whatever you set it to be in step 3.

7) Update the `sites.php` file to reflect the new site.

```
$sites = array (
  'dept187.lndo.site' => 'dev.starter-clone-test.o1.its-hosting-187.fcoe.org',
  'SITENAME.lndo.site' => 'SITENAME.fcoe.org'
);
```

## Upgrading the Site (The Hard Part) ##

The process for upgrading the site attempts to upgrade the site to a codebase that's already been upgrading, what an Aegir migrate does out of the box. This creates several problems because the database config and site config won't match the new drupal version and module versions out of the gate.

Loading up the new site on your local build will bring a white screen of death.

The following list of steps are the commands that bring the site safely from A to B. TODO: Explain the meaning of each of these.

Step 1: Add a reference to `config/sync` in the settings file, and a 'sites/config/sync' folder.

Step 2: Run these commands within the `sites/SITENAME` folder.

```
lando drush cr all
lando drush pmu pathauto -y (you will get an error, ignore)
lando drush pmu pathauto -y
lando drush pmu insert -y
lando drush pmu redis -y
lando drush up (cancel when asked to run the update)
```

**IMPORTANT** (then cancel the update before it runs the core update) **IMPORTANT**

Step 4: At this point you should cancel the drush up when asked to execute. This will then run a series of updates that are run even if the `drush up` is cancelled.

```
lando drush cr all
lando drush updb
lando drush en insert pathauto -y
lando drush en fcss_dept_ct__page fcss_dept_ct__event -y
lando drush en fcss_dept_vt__event -y
lando drush en fcss_dept_deploy -y
lando drush updb -y
lando drush updb -ys
```

Step 5: Open the site to ensure that it is running correctly and on the correct platform. Errors for title, insert, and pathauto should disappear in the status report, but parts of the site will need to be reconfigured on Aegir.

This is usually the point where the site is reconfigured to play nice with the new theme and Layout Builder. It's best, however, to forgo this step push the site remotely. Once the site is in Aegir, follow the steps below to update the site. I've found that some are overwritten if the site is pushed up afterward. This may be due to a mismatch in the features that occurs during the upgrade process.

## Creating and Importing the Local Site ##

This is fairly straightforward. Build a site on the 01.07_c platform, import the local DB to the site, and rsync the files

## Configuring the Live Site Before launch ##

This process follows the guidelines in the `Upgrade.md` file in this repo, but adds some steps that occur as a result of the non-aegir update process. These changes revolve primarily around pathauto and insert.

### Enabling Pathauto ###

Run the `drush en pathauto` command and hope the site doesn't explode (it shouldn't). Once it's enabled, you'll need to recreate the two pathauto patterns that exist in the dev starter site, One for pages and one for galleries.

### Enabling Insert ###

likewise, run the `drush en insert` command. Similar to pathauto, insert made a breaking change to how it approaches file insert, which requires a lot of migration during the `drush up` command for the module. We can't run this command, however, in the local build, and so the fields that run insert need to be randomly recreated.

### Checking for Disabled Fields ###

Because of the changes to field types and the insert issues spelled out above, some fields in the `form display` for content types and block types are disabled by Drupal as a precautionary measure. These are, to my knowledge, body fields and any field that uses the `insert` module. For Stem, this amounted to six fields, but it required me to review the form display settings for each content type to make sure. The process should only take about 10 minutes, but is absolutely essential, as the user will not be able edit content.


## Post Deployment Fixes (borrowed from Upgrade.md) ##

#### Resetting The Block Layout
 
All nodes should use layout builder to place blocks, so several need to be disabled or moved to accommodate the new layouts. The skeleton of the block layout should appear as follows. If there are any styling errors, check that the machine readable names match those below.

**Header**

* Site Branding `fcss_dept_sitebranding`
* Main Navigation `fcss_dept_main_menu` **Do not use** `mainnavigation`

**Content**

* Help `fcss_dept_help`
* Tabs `tabs` **or** `fcss_dept_local_tasks`
* Main Page Content `fcss_dept_content`
* Page Title (disable) `fcss_dept_page_title`

All other blocks may be disabled or removed, based on the site. If there are any content types that have layout builder deactivated, restrict blocks to only appear on those content types. 

You may need to configure the `Main Navigation` menu block to only display the first level of the menu, which may be set to 'Unlimited'.

#### Manual Configuration Changes ####

1. The 'Quick Edit' module should be deactivated. **TODO:** Run this through HOOK_update

### Setting Up Site Content ###
*****
Both Deploy scripts should configure content types to display similar to the previous theme, without any customizing. There are a few exceptions

1.	**Recreate the Homepage:** Create a page titled 'Home' and customize the layout. Set the 'Home' page to be the default home page in Site Settings.

2. **Re-Place or Recreate Blocks within Layout Builder**

3. **Duplicate Views Pages into Views Blocks and place them on a Basic Page** Because of the styling and layout requirements of layout builder, it's easier to create a page and include a duplicate block than to have a views page, although this isn't required at all times.

## Notes on Features ##

Because this process is fairly intensive, these steps are meant to be global, and shouldn't require adjustment on a site by site basis. There can be, however, additional steps involving features which need to be addressed on a site by site basis.

Currently, there's a mismatch on features that throws warnings within Drupal. The mismatch is related to some features that have been updated but aren't in the platform. These updates were to fix issues with the title depencancy and insert irregularities. Because this mismatch persists, the deployment script does not currently import those changes. I have made the changes, they simply need to be pushed to the repo and references in the repo script uncommented.

Fixing the feature mismatch can be done globally on all sites at any time, and will not impede the migration process.
