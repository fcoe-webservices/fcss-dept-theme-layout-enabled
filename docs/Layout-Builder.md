# Using Layout Builder

### Configuration ###
***** 
To enable layout for a specific content type, visit the `Display` tab for the content type, and enable layout builder via the check mark at the bottom of the page. Once layout builder has been activated, all nodes will automatically switch over to the default layout.

#### Default Layouts ####
Each content type has a default layout that sets the starting point for new and existing nodes. If a node has not been customized, all modifications to the default will be reflected in that node's layout. Customized layouts, however, will not receive the changes.

#### Customized Layouts ####
You can also provide users with the ability to override a default layout on a node by node basis. Once a customized layout is created, you cannot deactivate layout builder for the content type.

#### Restricting Access to Content or Layouts ####
The [Layout Builder Restrictions](https://www.drupal.org/project/layout_builder_restrictions) provides two accordian menus on the content type display tab for disabling content (fields, blocks, views, pages, etc) and layouts. The restrictions are content type specific, and can't be overriden on a single entity, no matter the user permissions. 


## Custom Blocks ##
These blocks are available within the `Custom Blocks` section when adding content to the layout. Custom block content is not persistent. Once it's deleted, it's relegated to the dustbin of the web. Add custom blocks to the default layout then override a node by node basis if you'd like to display them across the site.

### Content Notice (Horizontal) ###
*****
![Horizontal Content Notice](../images/README/content-notice-horizontal.png)

### Content Notice (Vertical) ###
*****
![Vertical Content Notice](../images/README/content-notice-vertical.png)

### Gallery (Horizontal) ###
*****
![Vertical Gallery](../images/README/gallery-vertical.png)

### Gallery (Vertical) ###
*****
![Horizontal Gallery](../images/README/gallery-horizontal.png)

### Single Contact (Horizontal) ###
*****
![Horizontal Gallery](../images/README/single-contact-horizontal.png)

### Single Contact (Vertical) ###
*****
![Horizontal Gallery](../images/README/single-contact-vertical.png)

### Contact List (Horizontal) ###
*****
![Horizontal Gallery](../images/README/contact-list-horizontal.png)

### Location (Vertical) ###
*****
![Horizontal Gallery](../images/README/single-event-vertical.png)
![Horizontal Gallery](../images/README/single-event-vertical-1.png)

### Single News (Horizontal) ###
*****
![Horizontal Gallery](../images/README/single-news-horizontal.png)

### News List (Vertical) ###
*****
![Horizontal Gallery](../images/README/news-list-horizontal.png)

### Image Callout (Horizontal) ###
*****
![Image Callout](../images/README/image-callout.png)

### Image List Item (Horizontal) ###
*****
![Horizontal Gallery](../images/README/image-list-item.png)

### Link List (Vertical) ###
*****
![Vertical Link-List](../images/README/link-list-vertical.png)

### Text Block ###
*****
![Vertical Link-List](../images/README/text-block.png)

### Hero (Large) ###
*****
![Hero large](../images/README/hero-large.png)

### Hero (Medium) ###
*****
![Hero medium](../images/README/hero-medium.png)

### Hero (Small) ###
*****
![Hero small](../images/README/hero-small.png)

### Event List (Vertical) ###
*****
![Vertical Gallery](../images/README/event-list-vertical.png)


[comment]: <> (Event List Horizontal/News Vertical/Single Event(h and v))