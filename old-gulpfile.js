var gulp            = require('gulp'),
    $               = require('gulp-load-plugins')(),
    browserSync     = require('browser-sync'),
    importOnce      = require('node-sass-import-once'),
    concat          = require('gulp-concat'),
    // pathing
    sassPaths = [
        'bower_components/foundation-sites/scss',
        'bower_components/motion-ui/src',
        'sass/',
    ],
    sassFiles = [
      'sass/**/*.scss',
      '!sass/**/_*.scss',
    ],

    // Error notifications in console
    reportError = function(error) {
        $.notify({
            title: 'Gulp Task Error',
            message: 'Check the console.'
        }).write(error);
        console.log(error.toString());
        this.emit('end');
    };

// Sass processing
gulp.task('sass', function() {
  return gulp.src(sassFiles)
    .pipe($.sass({
        importer: importOnce,
        includePaths: sassPaths,
        outputStyle: 'nested',
        precision: 10
    })
    .on('error', $.sass.logError))
    .on('error', reportError)
    .pipe($.autoprefixer({
        browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe($.rename({dirname: ''}))
    .pipe(concat('app.css'))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.stream());
});

// BrowserSync
gulp.task('browser-sync', function() {
    browserSync.init({
      proxy: "https://selpa.lndo.site",
      port: 3000,
    });
});

// Default
gulp.task('default', ['sass', 'browser-sync'], function() {
    gulp.watch(['css/lb-overrides.css','sass/**/*.scss','templates/**/*.twig'], ['sass']);
    //gulp.watch(['templates/**/*.twig'], ['drush-reload']);
});











// The following tasks are in development

// Optimize Images
gulp.task('images', function() {
    return gulp.src('images/**/*')
    .pipe($.imagemin({
        progressive: true,
        interlaced: true,
        svgoPlugins: [{
            cleanupIDs: false
        }]
    }))
    .pipe(gulp.dest('images'));
});

// JS hint
gulp.task('jshint', function() {
    return gulp.src('scripts/*.js')
    .pipe(reload({
        stream: true,
        once: true
    }))
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.notify({
        title: "JS Hint",
        message: "JS Hint says all is good.",
        onLast: true
    }));
});

// Beautify JS
gulp.task('beautify', function() {
    gulp.src('scripts/*.js')
    .pipe($.beautify({indentSize: 2}))
    .pipe(gulp.dest('scripts'))
    .pipe($.notify({
        title: "JS Beautified",
        message: "JS files in the theme have been beautified.",
        onLast: true
    }));
});

// Compress JS
gulp.task('compress', function() {
    return gulp.src('scripts/*.js')
    .pipe($.sourcemaps.init())
    .pipe($.uglify())
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('scripts'))
    .pipe($.notify({
        title: "JS Minified",
        message: "JS files in the theme have been minified.",
        onLast: true
    }));
});
