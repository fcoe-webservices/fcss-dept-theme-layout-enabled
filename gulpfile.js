//// Gulpfile Version 0.8
// For use with Zurb Foundation in Drupal 9

const {src, dest, watch, series} = require('gulp');
const sass = require('gulp-sass')(require('sass-embedded'));
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const terser = require('gulp-terser');
const browser = require('browser-sync').create();

//// Load Settings from config.yml
// Updated method used based on zurb template
const yaml = require('js-yaml');
const fs = require('fs-extra');

function loadConfig() {
    const unsafe = require('js-yaml-js-types').all;
    const schema = yaml.DEFAULT_SCHEMA.extend(unsafe);
    const ymlFile = fs.readFileSync('config.yml', 'utf8');
    return yaml.load(ymlFile, {schema});
  }

const { BROWSERSYNC, PATHS } = loadConfig();

//// Gulp Sass Build Task
// Build scss -> css with sass-embedded
function buildTask(){
    return src(PATHS.scss, {sourcemaps: false })
        .pipe(sass({
            includePaths: PATHS.foundationScss
        })
        //.on('error', sass.logError)
        )
        //.pipe(postcss([cssnano()]))
        .pipe(dest('css'))
        .pipe(browser.stream());
}

//// Javascript Task
// Using terser to compile
function jsTask(){
    return src(PATHS.js, {sourcemaps: true })
        .pipe(terser())
        .pipe(dest('dist', {sourcemaps: '.' }));
}

//// Browsersync Initialize Server Task
// Initialize Browsersync server with project URL
function browsersyncServe(cb) {
    browser.init({
        port: 3055,
        proxy: {
            target: BROWSERSYNC
        },
        open: false,
        ui: false
    }, cb());
}

//// Browsersync Reload Webpage
// Completely refresh the page
function browserReload(cb){
    browser.reload();
    cb();
}

//// Watch Tasks
function watchTask(){
    // reload on twig changes within the templates folder 
    watch(['templates/**/*.twig'], browserReload);
    // If js is changed then a page reload is needed
    watch(['js/*.js'], series(jsTask, browserReload));
    // If scss is changed then compile and stream
    watch(['scss/*/*.scss'], buildTask);
    // If @imports or $variables are changed then a page reload is needed
    watch(['scss/*.scss'], series(buildTask, browserReload))
}

//// Default Gulp Tasks
exports.default = series(
    //jsTask,
    buildTask,
    browsersyncServe,
    watchTask
)
